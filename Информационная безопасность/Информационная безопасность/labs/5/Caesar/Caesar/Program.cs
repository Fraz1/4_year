﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Caesar
{
    class Program
    {
        static void Main(string[] args)
        {
            string lang = "ru";
            string text = File.ReadAllText($@"text_{lang}.txt");
            var teoreticalFreqs = ParseFreqsFile($"freqs_{lang}_fixed.txt");
            var practicalFreqs = CalculateCharFrequences(text);
            var replacements = GetCharReplacements(teoreticalFreqs, practicalFreqs);
            var decryptedText = ReplaceChars(text, replacements);
            Console.WriteLine(decryptedText);
            File.WriteAllText("res.txt", decryptedText);
        }

        private static Dictionary<char, double> ParseFreqsFile(string path)
            => File
                .ReadAllLines(path)
                .Select(line => line.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries))
                .ToDictionary(pair => pair[1][0], pair => double.Parse(pair[0], CultureInfo.InvariantCulture));

        private static Dictionary<char, double> CalculateCharFrequences(string text)
        {
            var res = new Dictionary<char, double>();
            foreach (char c in text)
            {
                if (char.IsLetter(c))
                {
                    if (res.ContainsKey(c))
                        res[c]++;
                    else res[c] = 1;
                }
            }

            return res
                .Select(pair => new KeyValuePair<char, double>(pair.Key, pair.Value / text.Length))
                .OrderByDescending(i => i.Value)
                .ToDictionary(i => i.Key, i => i.Value);
        }

        private static Dictionary<char, char> GetCharReplacements(
            Dictionary<char, double> teoreticalFreqs,
            Dictionary<char, double> practicalFreqs)
            => teoreticalFreqs
                .Zip(practicalFreqs, (p1, p2) => new KeyValuePair<char, char>(p2.Key, p1.Key))
                .ToDictionary(p => p.Key, p => p.Value);

        private static string ReplaceChars(string text, IReadOnlyDictionary<char, char> charReplacements)
        {
            var chars = text.ToCharArray();
            for (int i = 0; i < chars.Length; i++)
            {
                if (!charReplacements.ContainsKey(chars[i])) continue;

                chars[i] = charReplacements[chars[i]];
            }

            return string.Join("", chars);
        }

        [Obsolete]
        private static void FormatParsedFreqs()
        {
            var freqs = GetRuFreqs();
            var f = string.Join(Environment.NewLine, freqs
                .OrderByDescending(i => i.Value)
                .Select(i => $"{i.Value:F4} {i.Key}"));
            Console.WriteLine(f);
        }

        [Obsolete]
        private static Dictionary<char, double> GetRuFreqs()
        {
            var lines = File.ReadAllLines(
                @"d:\Универ\Year_4\Информационная безопасность\Информационная безопасность\labs\5\freq.txt");
            var res = new Dictionary<char, double>();
            for (var i = 0; i < lines.Length; i += 3)
            {
                char c = char.ToLower(lines[i + 1][0]);
                double freq = double.Parse(lines[i + 2]) / 100;
                res[c] = freq;
            }

            return res;
        }

        [Obsolete]
        private static Dictionary<char, double> GetEnFreqs()
        {
            var lines = File.ReadAllLines(
                @"d:\Универ\Year_4\Информационная безопасность\Информационная безопасность\labs\5\freqEn.txt");

            var res = new Dictionary<char, double>();
            for (var i = 0; i < lines[0].Length; i++)
            {
                try
                {
                    char c = char.ToLower(lines[0].Split(" ")[i][0]);
                    double freq = double.Parse(lines[1].Split(" ")[i]) / 100;
                    res[c] = freq;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            return res;
        }
    }
}