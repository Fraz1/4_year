using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;

namespace steganography
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static class Extensions
    {
        // Note this MODIFIES THE GIVEN ARRAY then returns a reference to the modified array.
        private static byte[] Reverse(this byte[] b)
        {
            Array.Reverse(b);
            return b;
        }

        public static UInt16 ReadUInt16BE(this BinaryReader binRdr)
        {
            return BitConverter.ToUInt16(binRdr.ReadBytesRequired(sizeof(UInt16)).Reverse(), 0);
        }

        public static Int16 ReadInt16BE(this BinaryReader binRdr)
        {
            return BitConverter.ToInt16(binRdr.ReadBytesRequired(sizeof(Int16)).Reverse(), 0);
        }

        public static UInt32 ReadUInt32BE(this BinaryReader binRdr)
        {
            return BitConverter.ToUInt32(binRdr.ReadBytesRequired(sizeof(UInt32)).Reverse(), 0);
        }

        public static Int32 ReadInt32BE(this BinaryReader binRdr)
        {
            return BitConverter.ToInt32(binRdr.ReadBytesRequired(sizeof(Int32)).Reverse(), 0);
        }

        public static byte[] ReadBytesRequired(this BinaryReader binRdr, int byteCount)
        {
            var result = binRdr.ReadBytes(byteCount);

            if (result.Length != byteCount)
                throw new EndOfStreamException(string.Format("{0} bytes required from stream, but only {1} returned.",
                    byteCount, result.Length));

            return result;
        }

        public static byte[][] GetColorBytes(this byte[] input)
        {
            return input.Batch(3).Select(b => b.ToArray()).ToArray();
        }

        public static bool[] ToBoolArray(this byte[] input, bool reverse = false)
        {
            return input
                .Select(ToBitArray)
                .Select(b => b.ToBooleanArray(reverse))
                .SelectMany(bit => bit)
                .ToArray();
        }

        private static BitArray ToBitArray(this byte b)
        {
            return new BitArray(new[] {b});
        }

        public static bool[] ToBooleanArray(this BitArray a, bool reverse = false)
        {
            bool[] bits = new bool[a.Count];
            a.CopyTo(bits, 0);
            return reverse ? bits.Reverse().ToArray() : bits;
        }

        public static bool[] ToBooleanArray(this byte b, bool reverse = false)
        {
            return b.ToBitArray().ToBooleanArray();
        }

        public static byte ToByte(this bool[] bits)
        {
            if (bits.Length != 8)
            {
                throw new ArgumentException(nameof(bits));
            }
            var bitArray = new BitArray(bits);
            var bytes = new byte[1];
            bitArray.CopyTo(bytes, 0);
            return bytes[0];
        }

        public static byte[] ToByteArray(this bool[] bits)
        {
            return bits
                .Batch(8)
                .Select(b => b.ToArray())
                .Select(b => b.ToByte())
                .ToArray();
        }
        
        public static byte SetLastBit(this byte input, bool bit)
        {
            var bitArray = input.ToBooleanArray();
            bitArray[0] = bit;
            
            return bitArray.ToByte();
        }

        public static bool GetLastBit(this byte input)
        {
            return input.ToBooleanArray()[0];
        }

    }
}