using System.IO;

namespace steganography
{
    public class PngParser
    {
        public static PngFormat ParsePng(Stream stream)
        {
            var res = new PngFormat();
            using (var br = new BinaryReader(stream))
            {
                res.Header = ParseHeader(br);
                
                while (stream.Position < stream.Length)
                {
                    res.Chunks.Add(ParseChunk(br));
                }
            }

            return res;
        }

        private static PngHeader ParseHeader(BinaryReader r)
        {
            var bytes = r.ReadBytes(8);
            return new PngHeader(bytes);
        }

        private static PngChunk ParseChunk(BinaryReader r)
        {
            var length = r.ReadBytes(4);
            var res = new PngChunk(length);

            res.Header = r.ReadBytes(4);
            res.Data = r.ReadBytes((int)res.LenghtInt);
            res.Crc32=  r.ReadBytes(4);
            return res;
        }
    }
}