using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace steganography
{
    public class PngFormat
    {
        public PngHeader Header { get; set; }
        public List<PngChunk> Chunks { get; set; } = new List<PngChunk>();

        public PLTEChunk PLTEChunk {
            get {
                var chunk = Chunks.FirstOrDefault(c => c.HeaderAscii == "PLTE");
                return chunk == null ? null : new PLTEChunk(chunk);
            }
        }
    }

    public class PngHeader
    {
        public int Length => 8;
        public byte[] Data { get; }

        public PngHeader(byte[] data)
        {
            Data = data;
        }
    }

    public class PngChunk
    {
        public byte[] Length { get; set; }

        public uint LenghtInt => BitConverter.ToUInt32(Length.Reverse().ToArray());
        public byte[] Header { get; set; }
        public string HeaderAscii => Encoding.ASCII.GetString(Header);
        public byte[] Data { get; set; }
        public byte[] Crc32 { get; set; }

        public PngChunk(byte[] length)
        {
            Length = length;
        }

        public PngChunk(byte[] length, byte[] header, byte[] data, byte[] crc32) : this(length)
        {
            Header = header;
            Data = data;
            Crc32 = crc32;
        }

        public override string ToString()
        {
            return HeaderAscii;
        }
    }

    public class PLTEChunk
    {
        public PngChunk InnerChunk { get; }
        public Color[] ColorTable { get; }
        public byte[][] ColorBytes { get; }

        public PLTEChunk(PngChunk innerChunk)
        {
            InnerChunk = innerChunk;
            ColorBytes = innerChunk.Data
                .Batch(3)
                .Select(b => b.ToArray())
                .ToArray();
            ColorTable = ColorBytes.Select(c => Color.FromArgb(c[0], c[1], c[2])).ToArray();
        }

        public void SetColorBytes(byte[][] bytesArrays)
        {
            InnerChunk.Data = bytesArrays.SelectMany(a => a).ToArray();
        }
    }
}