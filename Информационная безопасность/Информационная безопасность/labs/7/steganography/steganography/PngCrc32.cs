using System;
using System.Linq;

namespace steganography
{
    public class PngCrc32
    {
        private uint[] _crcTable;

        // Stores a running CRC (initialized with the CRC of "IDAT" string). When
        // you write this to the PNG, write as a big-endian value
        private uint _idatCrc;

        public PngCrc32()
        {
            _idatCrc = Crc32(new[] {(byte) 'I', (byte) 'D', (byte) 'A', (byte) 'T'}, 0, 4, 0);
        }

        // Call this function with the compressed image bytes, 
        // passing in idatCrc as the last parameter
        private uint Crc32(byte[] stream, int offset, int length, uint crc)
        {
            uint c;
            if (_crcTable == null)
            {
                _crcTable = new uint[256];
                for (uint n = 0; n <= 255; n++)
                {
                    c = n;
                    for (var k = 0; k <= 7; k++)
                    {
                        if ((c & 1) == 1)
                            c = 0xEDB88320 ^ ((c >> 1) & 0x7FFFFFFF);
                        else
                            c = ((c >> 1) & 0x7FFFFFFF);
                    }

                    _crcTable[n] = c;
                }
            }

            c = crc ^ 0xffffffff;
            var endOffset = offset + length;
            for (var i = offset; i < endOffset; i++)
            {
                c = _crcTable[(c ^ stream[i]) & 255] ^ ((c >> 8) & 0xFFFFFF);
            }

            return c ^ 0xffffffff;
        }

        public byte[] CalculatePngCrc32(byte[] input)
        {
            uint crc32 = Crc32(input, 0, input.Length, _idatCrc);

            var reversedCrc32 = BitConverter.GetBytes(crc32).Reverse().ToArray();
            return reversedCrc32;
        }
    }
}