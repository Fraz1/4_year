using System.IO;
using System.Linq;

namespace steganography
{
    public class PngWriter
    {
        public static MemoryStream CreateStream(PngFormat png)
        {
            var dataArray = CreateByteArray(png);
            return new MemoryStream(dataArray);
        }

        public static byte[] CreateByteArray(PngFormat png)
        {
            var dataArray = png.Header.Data.Concat(
                    png.Chunks.Select(c => c.Length.Concat(c.Header.Concat(c.Data).Concat(c.Crc32))).SelectMany(b => b))
                .ToArray();
            return dataArray;
        }
    }
}