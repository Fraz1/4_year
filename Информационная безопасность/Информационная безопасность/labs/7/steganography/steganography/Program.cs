﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace steganography
{
    class Program
    {
        static void Main(string[] args)
        {
//            string path = args[0];
//
//            if (args[1] =="-e")
//            {
//                string message = args[2];
//                RunHide(path, message, "output.png");
//            }
//            else if (args[1] == "-d")
//            {
//                int len = int.Parse(args[2]);
//                RunRead(path, len);
//            ;

            string containerPath = "container.png";
            string outputPath = "output.png";
            
            string msg = "I love pandas and cats";
            int msgLen = msg.Length;
            RunHide(containerPath, msg, outputPath);
            RunRead(outputPath, msgLen);
        }

        private static void RunRead(string path, int length)
        {
            var png = PngParser.ParsePng(File.OpenRead(path));
            var plte = png.PLTEChunk;

            var msgBytes = ReadMessageV2(plte.ColorBytes, length);
            Console.WriteLine(Encoding.ASCII.GetString(msgBytes));
        }

        private static void RunHide(string path, string message, string outpath)
        {
            var crcCalculator = new PngCrc32();
            var messageBytes = Encoding.ASCII.GetBytes(message);

            var png = PngParser.ParsePng(File.OpenRead(path));
            var block = GetDataContainerChunk(png);
            var colorBytes = block.Data.GetColorBytes();
            
            HideMessageV2(messageBytes, colorBytes);
            block.Data = colorBytes.SelectMany(b => b).ToArray();
            block.Crc32 = crcCalculator.CalculatePngCrc32(block.Data);
            
            var outputBytes = PngWriter.CreateByteArray(png);
            File.WriteAllBytes(outpath, outputBytes);
        }


        private static void HideMessageV2(byte[] messageBytes, byte[][] colorBytes)
        {
            var messageBits = messageBytes.ToBoolArray();
            for (var i = 0; i < messageBits.Length; i++)
            {
                byte b = colorBytes[i][2];
                colorBytes[i][2] = b.SetLastBit(messageBits[i]);
            }
        }

        private static byte[] ReadMessageV2(byte[][] colorBytes, int lengthBytes)
        {
            int bitsLength = lengthBytes * 8;
            bool[] bitsArray = new bool[bitsLength];
            for (var i = 0; i < bitsLength; i++)
            {
                bitsArray[i] = colorBytes[i][2].GetLastBit();
            }

            return bitsArray.ToByteArray();
        }

        private static PngChunk GetDataContainerChunk(PngFormat png)
        {
            return png.PLTEChunk?.InnerChunk ?? png.Chunks
                       .First(c => c.HeaderAscii == "IDAT");
        }
    }
}