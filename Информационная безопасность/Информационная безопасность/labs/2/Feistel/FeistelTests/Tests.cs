﻿using System;
using System.Collections.Generic;
using System.Linq;
using Feistel;
using NUnit.Framework;

namespace FeistelTests
{
    [TestFixture]
    public class FeistelTests
    {
        public IEnumerable<FeistelEncryptor> Encryptors { get; set; }

        [SetUp]
        public void SetUp()
        {
            var rnd = new Random();
            Encryptors = Enumerable.Range(0, 1000)
                .Select(_ => new FeistelEncryptor(new FknFeistelTransformer(), rnd.NextLong()));
        }

        [TearDown]
        public void TearDown()
        {
            Encryptors = null;
        }

        [Test]
        public void Test1()
        {
            foreach (var e in Encryptors)
            {
                Test(e, "Test 1 Test 2 Test 3 Test 4 Test 5 Test 6 Test 7");
            }

        }

        private static void Test(FeistelEncryptor encryptor, string input)
        {
            Console.WriteLine($"KEY: {encryptor.Key}");
            var encrypt = encryptor.Encrypt(input);
            string decrypt = encryptor.DecryptToString(encrypt);
            Assert.AreEqual(input, decrypt);
        }
    }
}