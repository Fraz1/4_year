﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Feistel
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var rnd = new Random();
            string message = "Test 1 Test 2 Test 3 Test 4 Test 5 Test 6 Test 7";

            const int blockSize = 8;
            int rounds = 10;
            ulong key = rnd.NextLong();
            byte[] iv = rnd.NextBytes(blockSize);

            Console.WriteLine($"KEY: {key}, IV: {iv}");

            var encoding = Encoding.Unicode;
            var transformer = new FknFeistelTransformer();
//            var encryptionAlghoritm = new CBCFeistelAlghoritm(blockSize, key, rounds, iv, transformer);
            var encryptionAlghoritm = new CFBAlghoritm(blockSize, key, rounds, iv, transformer);
//            var encryptionAlghoritm = new DefaultFeistelAlghoritm(blockSize, key, rounds, transformer);
            var feistel = new FeistelEncryptor(blockSize, encryptionAlghoritm, encoding);
            var encrypted = feistel.Encrypt(message);
            var decrypted = feistel.DecryptToString(encrypted);
            Console.WriteLine($"Plain text:{message}");
            Console.WriteLine($"Encrypted text:{encoding.GetString(encrypted)}");
            Console.WriteLine($"Raw bytes      :{encoding.GetBytes(message).Select(b => b.ToString("000")).JoinToString(", ")}");
            Console.WriteLine($"Encrypted bytes:{encrypted.Select(b => b.ToString("000")).JoinToString(", ")}");
            Console.WriteLine(decrypted);

        }
    }
}