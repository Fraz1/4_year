using System;
using System.Collections.Generic;
using System.Linq;

namespace Feistel
{
    public class DefaultFeistelAlghoritm : IEncryptionAlghoritm
    {
        public int BlockSize { get; }
        public ulong Key { get; }

        public IFeistelTransformer Transformer { get; }

        public int Rounds { get; }

        public DefaultFeistelAlghoritm(int blockSize, ulong key, int rounds, IFeistelTransformer transformer)
        {
            BlockSize = blockSize;
            Key = key;
            Rounds = rounds;
            Transformer = transformer;
        }

        public virtual byte[] Encrypt(byte[] input)
        {
            return EncryptDecrypt(input, false);
        }

        public virtual byte[] Decrypt(byte[] input)
        {
            return EncryptDecrypt(input, true);
        }

        private byte[] EncryptDecrypt(byte[] input, bool reverse)
        {
            return input
                .Chunks(BlockSize)
                .Select(c => c.ToArray())
                .Select(a => SymmetricBlock.FromByteArray(a, BlockSize))
                .Select(b => EncryptBlock(b, reverse))
                .Select(block => block.ToBytes())
                .Aggregate<IEnumerable<byte>>((result, block) => result.Concat(block))
                .ToArray();
        }

        protected SymmetricBlock EncryptBlock(SymmetricBlock block, bool reverse)
        {
            int round = reverse
                ? Rounds
                : 1;

            for (int i = 0; i < Rounds; i++)
            {
                Console.WriteLine($"Round: {round}");
                var old = SymmetricBlock.Copy(block);

                uint roundKey = Key.RotateRight(round * 8)
                    .ToBytes()
                    .Take(sizeof(uint))
                    .ToArray()
                    .ToUInt32();

                uint transform = Transformer.Transform(block.Left, roundKey);

                Console.WriteLine($"Left: {block.Left} - {roundKey} -> Transform: {transform}");

                uint res = transform ^ block.Right;

                if (i < Rounds - 1)
                {
                    block.Right = block.Left;
                    block.Left = res;
                }
                else
                {
                    block.Right = res;
                }

                var new_ = SymmetricBlock.Copy(block);

                Console.WriteLine($"{old} - {roundKey} - {new_}");

                round += reverse ? -1 : 1;
                Console.WriteLine();
            }

            return block;
        }
    }
}