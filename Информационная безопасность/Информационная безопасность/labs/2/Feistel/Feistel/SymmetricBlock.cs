using System.Linq;

namespace Feistel
{
    public class SymmetricBlock
    {
        public uint Left { get; set; }
        public uint Right { get; set; }

        public SymmetricBlock(uint left, uint right)
        {
            Left = left;
            Right = right;
        }

        public byte[] ToBytes() => Left.ToBytes().Concat(Right.ToBytes()).ToArray();

        public static SymmetricBlock FromByteArray(byte[] arr, int blockSize)
        {
            uint left = arr
                .Take(blockSize / 2)
                .ToArray()
                .ToUInt32();

            uint right = arr
                .Skip(blockSize / 2)
                .Take(blockSize / 2)
                .ToArray()
                .ToUInt32();
            
            return new SymmetricBlock(left, right);
        }

        public static SymmetricBlock Copy(SymmetricBlock block)
        {
            return new SymmetricBlock(block.Left, block.Right);
        }

        public override string ToString()
        {
            return $"{nameof(Left)}: {Left}, {nameof(Right)}: {Right}";
        }
    }
}