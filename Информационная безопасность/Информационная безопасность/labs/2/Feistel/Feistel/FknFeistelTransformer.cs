namespace Feistel
{
    public class FknFeistelTransformer : IFeistelTransformer
    {
        public uint Transform(uint value, uint key)
        {
            return value.RotateLeft(9) ^ ~(key.RotateRight(11) & value);
        }
    }
}