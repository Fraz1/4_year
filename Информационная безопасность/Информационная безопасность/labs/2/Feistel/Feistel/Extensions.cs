using System;
using System.Collections.Generic;
using System.Linq;

namespace Feistel
{
    public static class Extensions
    {
        public static IEnumerable<IEnumerable<T>> Chunks<T>(this T[] array, int chunkSize)
        {
            for (var i = 0; i < (float) array.Length / chunkSize; i++)
                yield return array.Skip(i * chunkSize).Take(chunkSize);
        }

        public static string JoinToString<T>(this IEnumerable<T> enumerable, string separator)
            => string.Join(separator, enumerable);

        public static ulong NextLong(this Random rnd)
        {
            var buffer = new byte[8];
            rnd.NextBytes(buffer);
            return BitConverter.ToUInt64(buffer, 0);
        }

        public static byte[] NextBytes(this Random rnd, int num)
        {
            var buffer = new byte[num];
            rnd.NextBytes(buffer);
            return buffer;
        }

        public static uint RotateLeft(this uint value, int count)
            => (value << count) | (value >> (32 - count));

        public static ulong RotateLeft(this ulong value, int count)
            => (value << count) | (value >> (64 - count));

        public static uint RotateRight(this uint value, int count)
            => (value >> count) | (value << (32 - count));

        public static ulong RotateRight(this ulong value, int count)
            => (value >> count) | (value << (64 - count));

        public static uint ToUInt32(this byte[] array, int startIndex = 0)
            => BitConverter.ToUInt32(array, startIndex);

        public static byte[] ToBytes(this ulong number)
            => BitConverter.GetBytes(number);

        public static byte[] ToBytes(this uint number)
            => BitConverter.GetBytes(number);

        public static byte[] XOR(this byte[] arr1, byte[] arr2)
        {
            if (arr1.Length != arr2.Length)
                throw new ArgumentException($"{nameof(arr1)} and {nameof(arr2)} are not the same length");

            byte[] result = new byte[arr1.Length];

            for (int i = 0; i < arr1.Length; ++i)
                result[i] = (byte) (arr1[i] ^ arr2[i]);

            return result;
        }
        public static byte[] XOR(this byte[] arr1, byte[] arr2, int arr2Offset)
        {
            byte[] result = new byte[arr1.Length];

            for (int i = 0; i < arr1.Length; ++i)
                result[i] = (byte) (arr1[i] ^ arr2[i + arr2Offset]);

            return result;
        }
    }
}