namespace Feistel
{
    public interface IEncryptionAlghoritm
    {
        byte[] Encrypt(byte[] input);
        byte[] Decrypt(byte[] input);
    }
}