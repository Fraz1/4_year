namespace Feistel
{
    public interface IFeistelTransformer
    {
        uint Transform(uint value, uint key);
    }
}