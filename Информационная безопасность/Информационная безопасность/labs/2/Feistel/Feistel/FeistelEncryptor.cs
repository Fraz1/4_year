using System;
using System.Linq;
using System.Text;

namespace Feistel
{
    public class FeistelEncryptor
    {
        public IEncryptionAlghoritm EncryptionAlghoritm { get; }
        public int BlockSize { get; }
        public Encoding Encoding { get; }

        public FeistelEncryptor(int blockSize, IEncryptionAlghoritm encryptionAlghoritm)
            : this(blockSize, encryptionAlghoritm, Encoding.Unicode)
        {
        }

        public FeistelEncryptor(int blockSize, IEncryptionAlghoritm encryptionAlghoritm, Encoding encoding)
        {
            BlockSize = blockSize;
            Encoding = encoding;
            EncryptionAlghoritm = encryptionAlghoritm;
        }

        public byte[] Encrypt(string input)
            => Encrypt(Encoding.GetBytes(input));

        public byte[] Encrypt(byte[] input)
        {
            Console.WriteLine("Encrypt");
            int initialSize = input.Length;
            byte diff = (byte) (BlockSize - input.Length % BlockSize);
            Array.Resize(ref input, input.Length + diff);
            for (int i = initialSize; i < input.Length - 2; i++)
                input[i] = 0;
            input[input.Length - 1] = diff;
            return EncryptionAlghoritm.Encrypt(input);
        }

        public byte[] Decrypt(byte[] input)
        {
            Console.WriteLine("Decrypt");
            var res = EncryptionAlghoritm.Decrypt(input);
            byte padding = res.Last();
            Array.Resize(ref res, input.Length - padding);
            return res;
        }

        public string DecryptToString(byte[] input)
            => Encoding.GetString(Decrypt(input));

    }
}