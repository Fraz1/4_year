using System.Linq;

namespace Feistel
{
    class CFBAlghoritm : CBCFeistelAlghoritm
    {
        public CFBAlghoritm(int blockSize, ulong key, int rounds, byte[] iv, IFeistelTransformer transformer) 
            : base(blockSize, key, rounds, iv, transformer)
        {
        }

        public override byte[] Encrypt(byte[] input)
        {
            byte[] output = new byte[input.Length];

            byte[][] chunkArrays = input
                .Chunks(BlockSize)
                .Select(c => c.ToArray())
                .ToArray();


            for (int i = 0; i < chunkArrays.Length; i++)
            {
                var encryptBlock = EncryptBlock(SymmetricBlock.FromByteArray(
                    i == 0
                        ? IV
                        : output.Skip((i-1) * BlockSize).Take(BlockSize).ToArray(),
                    BlockSize),false);
                var encryptedBytes = encryptBlock.ToBytes();

                var cfb = encryptedBytes.XOR(input, i * BlockSize);
                cfb.CopyTo(output, i * BlockSize);
            }

            return output;

        }

        public override byte[] Decrypt(byte[] input)
        {
            byte[] output = new byte[input.Length];

            byte[][] chunkArrays = input
                .Chunks(BlockSize)
                .Select(c => c.ToArray())
                .ToArray();


            for (int i = 0; i < chunkArrays.Length; i++)
            {
                var decryptBlock = EncryptBlock(SymmetricBlock.FromByteArray(
                    i == 0
                        ? IV
                        : input.Skip((i-1) * BlockSize).Take(BlockSize).ToArray(),
                    BlockSize),false);
                var decryptBytes = decryptBlock.ToBytes();

                var cfb = decryptBytes.XOR(input, i * BlockSize);
                cfb.CopyTo(output, i * BlockSize);
            }

            return output;
        }
    }
}