using System.Linq;

namespace Feistel
{
    public class CBCFeistelAlghoritm : DefaultFeistelAlghoritm
    {
        public byte[] IV { get; }

        public CBCFeistelAlghoritm(int blockSize, ulong key, int rounds, byte[] iv, IFeistelTransformer transformer) :
            base(
                blockSize, key, rounds, transformer)
        {
            IV = iv;
        }

        public override byte[] Encrypt(byte[] input)
        {
            byte[] output = new byte[input.Length];

            byte[][] chunkArrays = input
                .Chunks(BlockSize)
                .Select(c => c.ToArray())
                .ToArray();


            for (int i = 0; i < chunkArrays.Length; i++)
            {
                byte[] cbc = i == 0
                    ? chunkArrays[0].XOR(IV)
                    : chunkArrays[i].XOR(output, (i - 1) * BlockSize);

                var encryptBlock = EncryptBlock(SymmetricBlock.FromByteArray(cbc, BlockSize), false);
                encryptBlock.ToBytes().CopyTo(output, i * BlockSize);
            }

            return output;
        }

        public override byte[] Decrypt(byte[] input)
        {
            byte[] output = new byte[input.Length];

            byte[][] chunkArrays = input
                .Chunks(BlockSize)
                .Select(c => c.ToArray())
                .ToArray();


            for (int i = 0; i < chunkArrays.Length; i++)
            {
                var decryptBlock = EncryptBlock(SymmetricBlock.FromByteArray(chunkArrays[i], BlockSize), true);
                var decryptBytes = decryptBlock.ToBytes();

                byte[] cbc = i == 0
                    ? decryptBytes.XOR(IV)
                    : decryptBytes.XOR(input, (i - 1) * BlockSize);
                
                cbc.CopyTo(output, i * BlockSize);
            }
            return output;
        }
    }
}