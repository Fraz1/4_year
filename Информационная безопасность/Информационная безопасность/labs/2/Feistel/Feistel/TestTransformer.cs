namespace Feistel
{
    class TestTransformer : IFeistelTransformer
    {
        public uint Transform(uint value, uint key)
        {
            uint u = (uint) key;

            checked
            {
                return value ^ u;
            }
        }
    }
}