﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using IS_PRNG.Properties;

namespace IS_PRNG
{
    internal class Program
    {

        private static int[] Primes;
        private static int _passedIterations;
        private static int _allIterations;

        public static void Main(string[] args)
        {
            Primes = File.ReadAllText(@"..\..\primes.txt")
                .Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries)
                .Where(s => s!= "\r\n")
                .Select(int.Parse)
                .ToArray();
            //var rnd = new LemerRandom(9, 3, 5);
            //int a1 = rnd.Next();
            //int a2 = rnd.Next();
            //int a3 = rnd.Next();
            var nonRepeatable = Task.Run(async () => await NonRepeatableNumbers(10000));

            while (true)
            {
                Console.ReadKey();
                Console.WriteLine($"Progress: {_passedIterations} / {_allIterations} - {_passedIterations / (double) _allIterations * 100:F1} ");
            }

            //nonRepeatable.Take(100).ForEach(r => Console.WriteLine($"U:{r.U}, P:{r.P}, M:{r.M}"));
            //nonRepeatable.ForEach(r => Console.WriteLine($"U:{r.U}, P:{r.P}, M:{r.M}"));

        }



        private static Task<List<LemerRandom>>NonRepeatableNumbers(int fence)
        {
            int uCount, pCount, mCount;
            uCount = mCount = 100;
            pCount = 25;
            _allIterations = uCount * pCount * mCount;
            var uEnumerable = Primes.Take(uCount);
            //var uEnumerable = Enumerable.Range(1, 255);
            var pEnumerable = Primes.Where(p => p >= fence).Take(pCount);
            //var pEnumerable = Enumerable.Range(1, 255);
            var mEnumerable = Primes.Take(mCount);
            //var mEnumerable = Enumerable.Range(1, 255);

            var testValues =
                uEnumerable.SelectMany(u =>
                        pEnumerable.SelectMany(p =>
                            mEnumerable.Select(m => (U: u, P: p, M: m))))
                    .ToList();

            var res = new ConcurrentQueue<LemerRandom>();

            Parallel.ForEach(testValues, v =>
            {
                var rand = new LemerRandom(v.U, v.M, v.P);
                bool success = rand.Take(fence).Distinct().Count() == fence;
                if (success)
                {
                    //Console.WriteLine($"U:{v.U}, P:{v.P}, M:{v.M}, res: {res.Count}");
                    res.Enqueue(rand);
                }

                Interlocked.Increment(ref _passedIterations);
            });
            //testValues
            //    .AsParallel()
            //    .ForEach(v =>
            //    {
            //        //if (v.U % 100 == 0 || v.P % 100 == 0 || v.M % 100 == 0)
            //        //{
            //        //    Console.WriteLine($"SUCCESS: U:{v.U}, P:{v.P}, M:{v.M}, res: {res.Count}");
            //        //}
            //        var rand = new LemerRandom(v.U, v.M, v.P);

            //        bool success = rand.Take(fence).ToList().Distinct().Count() >= fence;
            //        if (success)
            //        {
            //            //Console.WriteLine($"U:{v.U}, P:{v.P}, M:{v.M}, res: {res.Count}");
            //            res.Add(rand);
            //        }
            //    });
            const string path = @"..\..\res.txt";
            File.WriteAllLines(path, res.Select( v => $"SUCCESS: U:{ v.U}, P: { v.P}, M: { v.M}"));
            File.AppendAllText(path, $"COUNT: {res.Count}");
            return Task.FromResult(res.ToList());
        }
    }
}