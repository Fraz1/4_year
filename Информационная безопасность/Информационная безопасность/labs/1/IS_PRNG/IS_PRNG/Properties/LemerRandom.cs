using System.Collections;
using System.Collections.Generic;

namespace IS_PRNG.Properties
{
    public class LemerRandom : IEnumerable<int>
    {
        public int  U { get; private set; }

        public int M { get; }

        public int P { get; }

        public LemerRandom(int seed, int m, int p)
        {
            U = seed;
            M = m;
            P = p;
        }

        public int Next()
        { 
            int next = U * M % P;
            U = next;
            return next;
        }

        public double NextDouble()
        {
            return Next() / (double) P;
        }

        IEnumerator<int> IEnumerable<int>.GetEnumerator()
        {
            while (true)
                yield return Next();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (this as IEnumerable<int>).GetEnumerator();
        }
    }
}