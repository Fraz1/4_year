﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Numerics;
using System.Text;

namespace RSA
{
    internal struct Key
    {
        public long Exp { get; set; }
        public long N { get; set; }

        public Key(long exp, long n)
        {
            Exp = exp;
            N = n;
        }
    }

    internal class Program
    {
        public static void Main(string[] args)

        {
            const string message = "314999112281065205361706341517321987491098667";
            long n = 471090785117207;
            long e = 12377;
            
            bool fac = TryFactorize(n, out long p, out long q);
            long fn = (p - 1) * (q - 1);
            
            Console.WriteLine($"FAC: P:{p} Q:{q}, Fn:{fn}");
            Console.WriteLine(n > fn);
            
            long gcd = Egcd(e, fn, out var x, out var y);
            Console.WriteLine($"GCD: {gcd}, X: {x}, Y: {y}");

            long secretExp = (x + fn) % fn;
            Console.WriteLine(e * x % fn);
            var blocks = GetBlocks(message, n);
            var decryptedASCII = Decrypt(blocks, new Key(secretExp, n));
            Console.WriteLine(GetText(decryptedASCII));
        }

        private static List<long> GetBlocks(string encryptedText, long n)
        {
            var res = new List<long>();
            int curIndex = 0;
            while (curIndex < encryptedText.Length)
            {
                int len = n.ToString().Length;
                long num;
                while (!(long.TryParse(encryptedText.Substring(curIndex, len), out num)
                         && num < n))
                {
                    len--;
                }

                curIndex += len;
                res.Add(num);
            }

            return res;
        }

        private static string GetText(string bytes)
        {
            if (bytes.Length % 2 == 1) bytes = bytes + "0";
            var res = new StringBuilder();
            for (var i = 0; i < bytes.Length; i += 2)
            {
                char c = (char) int.Parse(bytes[i].ToString() + bytes[i + 1]);
                res.Append(c);
            }

            return res.ToString();
        }

        private static string Decrypt(IEnumerable<long> blocks, Key privateKey)
        {
            var decryptedBlocks = blocks
                .Select(b => BigInteger.ModPow(b, privateKey.Exp, privateKey.N))
                .Select(b => b.ToString());
            return string.Join("", decryptedBlocks);
        }

        private static long GcdExtended(long a, long b, out long x, out long y)
        {
            // Base Case 
            if (a == 0)
            {
                x = 0;
                y = 1;
                return b;
            }

            long gcd = GcdExtended(b % a, a, out long x1, out long y1);

            // Update x and y using results of recursive 
            // call
            checked
            {
                x = y1 - (b / a) * x1;
                y = x1;
            }

            return gcd;
        }


        private static long Egcd(long a, long b, out long x, out long y)
        {
            x = 0;
            y = 1;
            long u = 1, v = 0;
            while (a != 0)
            {
                long q = b / a;
                long r = b % a;
                long m = x - u * q;
                long n = y - v * q;


//                b,a, x,y, u,v = a,r, u,v, m,n
                b = a;
                a = r;
                x = u;
                y = v;
                u = m;
                v = n;
            }

            long gcd = b;


            return gcd;
        }

        private static bool TryFactorize(long n, out long p, out long q)
        {
            p = q = 0;
            for (int i = 2; i < (long) Math.Sqrt(n); i++)
            {
                if (n % i != 0) continue;
                bool prime = true;
                for (int j = 2; j < i - 1; j++)
                {
                    if (i % j != 0) continue;
                    prime = false;
                    break;
                }

                if (!prime || n % i != 0) continue;
                p = i;
                q = n / i;
                return true;
            }

            return false;
        }
    }
}