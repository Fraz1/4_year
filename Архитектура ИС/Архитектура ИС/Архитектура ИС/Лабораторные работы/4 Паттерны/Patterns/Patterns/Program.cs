﻿using System;

namespace Patterns
{
    class Program
    {
        public Program()
        {
            var logger = new ConsoleLogger();
            var applicationLoggerAdapter = new ApplicationLoggerAdapter(logger);
            var applicationContext = new ApplicationContext(applicationLoggerAdapter);
            applicationContext.DoWork();
        }
    }
    
    public class ApplicationContext
    {
        private readonly IApplicationLogger _logger;

        public ApplicationContext(IApplicationLogger logger)
        {
            _logger = logger;
        }

        public void DoWork()
        {
            try
            {
                //Do something                
            }
            catch (Exception e)
            {
                _logger.LogAppError(e);
            }
        }
    }

    public interface IApplicationLogger
    {
        void LogAppError(Exception e);
        void LogAppWarning(string message);
    }

    public class ApplicationLoggerAdapter : IApplicationLogger
    {
        private readonly ILogger _logger;

        public ApplicationLoggerAdapter(ILogger logger)
        {
            _logger = logger;
        }

        public void LogAppError(Exception e)
        {
            _logger.Error(e, string.Empty);
        }

        public void LogAppWarning(string message)
        {
            _logger.Warning(message);
        }
    }


    public interface ILogger
    {
        void Error(Exception e, string message);
        void Warning(string message);
    }
    
    public class ConsoleLogger: ILogger
    {
        public void Error(Exception e, string message)
        {
            Console.WriteLine($"[{DateTime.UtcNow}] Error:{message}{Environment.NewLine}{e.Message}");
        }

        public void Warning(string message)
        {
            Console.WriteLine($"[{DateTime.UtcNow}] Warning:{message}");
        }
    }
}