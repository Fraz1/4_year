using System;

namespace Patterns
{
    public class Program1
    {
        public Program1()
        {
            var rand = new Random(DateTime.UtcNow.Ticks);
            var state = rand.GetState();
            long number1 = rand.Next();
            rand.RestoreState(state);
            long number2 = rand.Next();

            Console.WriteLine(number1 == number2); //true
        }
    }

    public class Random
    {
        private long Seed { get; set; }

        public Random(long seed)
        {
            Seed = seed;
        }

        public long Next()
        {
            return Seed = Seed * 312897 % 7382197;
        }

        public RandomState GetState()
        {
            return new RandomState(Seed);
        }

        public void RestoreState(RandomState state)
        {
            Seed = state.Seed;
        }
    }

    public class RandomState
    {
        public long Seed { get; }

        public RandomState(long seed)
        {
            Seed = seed;
        }
    }
}