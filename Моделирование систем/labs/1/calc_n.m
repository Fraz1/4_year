function n=calc_n(start_n, b, c, alpha, dm)
diff = start_n;
n = start_n;
u = [];
while 1
    for i=1:diff
        %�������� ���������������� �������
        u(size(u)+1)=systemeqv(b,c);
    end
    d_wave = std(u)^2;
    n_res = round(d_wave / (alpha  * dm^2));
    if n_res <= n
        break;
    end
    diff = n_res - n;
    n = n_res;
end
return;