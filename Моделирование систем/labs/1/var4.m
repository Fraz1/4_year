clear all;
nf=2;
minf=[1 2];
maxf=[6 6];
%������������ �������� �������������� ����� ������������
%��� ����� ��������������
fracplan=fracfact('a b ab' )
N=2^nf;
fictfact=ones(N,1);
X=[fictfact fracplan]'
fraceks=zeros(N,nf);
for i=1:nf
    for j=1:N
        fraceks(j,i)=minf(i)+(fracplan(j,i)+1)*(maxf(i)-minf(i)) / 2;
    end
end
fraceks
%����������� ������������ ������������
%������� �������������� ��������� � ������ ����������
d_sigma=0.12;
alpha=0.06;

%���� �� ������������ ������������� ��������������� �����
for j=1:N
    b=fraceks(j,1); 
    c=fraceks(j,2);
    NE = calc_n(2, b, c, alpha, d_sigma);
    %���� �������������� ���������
    for k=1:NE
        %�������� ���������������� �������
        u(k)=systemeqv(b,c);
    end
    %������ ���������� (�������) �� ������� ����������
    Y(j)=mean(u);
    %������������ � ����������� ����������� � 12-� �����������
    %figure;
    %hist(u,12);
end
C=X*X';
b_=inv(C)*X*Y'
%������������ ����������� ������� ������� �� ���������
%�������� ��������
A=minf(1):0.1:maxf(1);
B=minf(2):0.1:maxf(2);
[k N1]=size(A);
[k N2]=size(B);
for i=1:N1
    for j=1:N2
        an(i)=2*(A(i)-minf(1))/(maxf(1)-minf(1))-1;
        bn(j)=2*(B(j)-minf(2))/(maxf(2)-minf(2))-1;
        %����������������� ����������� �������
    Yc(j,i)=b_(1)+an(i)*b_(2)+bn(j)*b_(3)+an(i)*bn(j)*b_(4);
    end
end
for i=1:N1
    for j=1:N2
        %�������� ����������� �������
%             Yo(j,i)=(A(i)^2)*exp(B(j)^2)*(exp(B(j)^2)-1);
            Yo(j,i) = A(i) * B(j);
    end
end
% �������������� ������������ � ���������� ������� 
[x,y]=meshgrid(A,B);
figure;
subplot(1,2,1),plot3(x,y,Yc),
xlabel('fact b'),
ylabel('fact c'),
zlabel('Yc'),
title('System output'),
grid on,
subplot(1,2,2),plot3(x,y,Yo),
xlabel('fact b'),
ylabel('fact c'),
zlabel('Yo'),
title('System output'),
grid on;
