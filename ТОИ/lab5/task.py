# coding=utf-8
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as ss


def parsen(x, training_set, h, kernel, ksi):
    tmp_sum = 0
    training_set_size = len(training_set)

    for i in range(0, training_set_size):
        diff = x - training_set[i]
        arg = np.matmul(np.matmul(diff, np.linalg.inv(ksi)), np.transpose(diff)) / h
        tmp_sum += kernel(arg, h)
    return tmp_sum / (training_set_size * h)


def kernel_gauss(r, h):
    return np.power(2 * np.pi, -0.5) * np.exp(-0.5 * r / h)


def kernel_triangle(r, h):
    abs_r = abs(r)
    if abs_r < 1:
        return 1 - abs_r
    return 0


def kernel_square(r, h):
    abs_r = abs(r)
    if abs_r > 0.5:
        return 0
    elif abs_r == 0.5:
        return 0.5
    else:
        return 1


def kernel_log(r, h):
    return np.power(np.exp(r) + 2 + np.exp(-r), -1)


def get_approximator(training_set, N_array, window_generator, ksi):
    def approximate(kernel):
        N_array_len = len(N_array)
        prob_dens_approx = np.empty([N_array_len, ], np.ndarray)
        for i in range(0, N_array_len):
            prob_dens_approx[i] = np.empty([N_array[i]], np.ndarray)
            current_set = training_set[i]
            h = window_generator(N_array[i])
            for k in range(0, N_array[i]):
                x = current_set[k]
                prob_dens_approx[i][k] = parsen(x, current_set, h, kernel, ksi)
        return prob_dens_approx

    return approximate


def calculate_errors(source_prob_dens, approx_prob_dens):
    source_len = len(source_prob_dens)
    errors = np.empty([source_len])
    for i in range(source_len):
        count = len(source_prob_dens[i])
        errors[i] = np.sqrt(np.sum(np.power(source_prob_dens[i] - approx_prob_dens[i], 2))) / count
    return errors


def get_window_generator(n, r):
    def generator(N):
        return np.power(N, -r / n)

    return generator


def run():
    n = 2
    N_array = np.arange(10, 300, 50)

    r = 0.17
    window_generator = get_window_generator(n, r)
    cov = [[1, 0], [0, 100]]
    mean = [1, 10]

    distribution = ss.multivariate_normal(mean, cov)  # нормальное распределение

    prob_dens = np.empty((N_array.size,), np.core.ndarray)
    samples = np.empty((N_array.size,), np.core.ndarray)

    for i in range(0, N_array.size):
        samples[i] = distribution.rvs(N_array[i])  # выборка
        prob_dens[i] = distribution.pdf(samples[i])  # плотности вероятностей

    cov_approximator = get_approximator(samples, N_array, window_generator, cov)
    identity_approximator = get_approximator(samples, N_array, window_generator, np.identity(n))

    approximations_gauss_cov_density = cov_approximator(kernel_gauss)
    errors_gauss_cov = calculate_errors(prob_dens, approximations_gauss_cov_density)

    approximations_gauss_identity_density = identity_approximator(kernel_gauss)
    errors_gauss_identity = calculate_errors(prob_dens, approximations_gauss_identity_density)

    approximations_density_triangle = identity_approximator(kernel_triangle)
    errors_triangle = calculate_errors(prob_dens, approximations_density_triangle)

    approximations_density_square = identity_approximator(kernel_square)
    errors_square = calculate_errors(prob_dens, approximations_density_square)

    approximations_density_log = identity_approximator(kernel_log)
    errors_log = calculate_errors(prob_dens, approximations_density_log)

    plt.plot(N_array, errors_gauss_cov, 'r', label='Gauss Covariance matrix')
    plt.plot(N_array, errors_gauss_identity, 'b', label='Gauss Identity matrix')
    plt.plot(N_array, errors_triangle, 'y', label='Triangle', markersize=70, linewidth=3)
    plt.plot(N_array, errors_square, 'm', label='Square')
    plt.plot(N_array, errors_log, 'g', label='Log')

    plt.xlabel('N')
    plt.ylabel('Errors')
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
               ncol=2, mode="expand", borderaxespad=0.)
    plt.show()


run()
