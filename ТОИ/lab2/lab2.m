%% Надралиев Андрей Владимирович 3 группа
%% Лабораторная работа 2
%% Синтез и анализ алгоритмов распознавания ГСВ с одинаковой  матрицей ковариации
clear all; close all;

%% 1.Задание исходных данных
n=3; M=3; % размерность признакового пространства  и число классов
K = 100; % количество статистических испытаний
m = [-5 1 0; 1 -4 8; 10 -2 1];
pw = [0.1, 0.4, 0.5];
np=sum(pw); pw=pw/np;

C = [3 1 1; 1 3 1; 1 1 3]; % матрица ковариации
C_ = C^-1;
D = C(1,1,1);

% 1.1. Визуализация исходной совокупности образов
N = K  *  M;
NN = zeros(M, 1);
for k = 1 : M - 1
    NN(k) = uint16(N  *  pw(k));
end
NN(M) = N - sum(NN);
label = {'bo', 'r+', 'go'};

IMS = [];   % общая совокупность образов 
figure; grid on; hold all; title('Исходные метки образов');
for i=1:M % цикл по классам    
    ims = repmat(transpose(m(i,:)), 1, NN(i))  + randncor(n,NN(i),C); % генерация К образов i-го класса
    plot3(ims(1,:),ims(2,:), ims(3,:), label{i}); % 3d график выбранного цвета
    IMS = [IMS, ims];   % добавление в общую совокупность образов
end
view(3);

%% 2.Расчет разделяющих функций и матрицы вероятностей ошибок распознавания
G=zeros(M,n+1); PIJ=zeros(M); l0_=zeros(M);
for i = 1 : M
    G(i,1:n)=(m(i,:) * C_);
    G(i,n+1)=-0.5 * m(i,:) * C_ * m(i,:)';
    for j=i+1:M
        l0_(i,j)=log(pw(j)/pw(i)); 
        h=0.5 * (m(i,:)-m(j,:)) * C_ * (m(i,:)-m(j,:))';
        sD=sqrt(2 * h);
        PIJ(i,j)=normcdf(l0_(i,j),h,sD);
        PIJ(j,i)=1-normcdf(l0_(i,j),-h,sD);
    end
    PIJ(i,i)=1-sum(PIJ(i,:));%нижняя граница вероятности правильного распознавания
end

% 2.1. Визуальзация результатов распознавания образов
figure; grid on; hold all; title('Результат классификации образов');
%for i = 1 : N %цикл по всем образам совокупности
%    z = [IMS(:, i); 1]; % значение очердного образа из общей совокупности
%    u=G * z + log(pw');% вычисление значения разделяющих функций
%    [ui,iai]=max(u);% определение максимума (iai - индекс класса)
%    plot3(IMS(1, i), IMS(2, i), IMS(3, i), label{iai});
%end
%view(3);

%% 3.Тестирование алгоритма методом статистических испытаний
x=ones(n+1,1); Pc_=zeros(M);%экспериментальная матрица вероятностей ошибок
for k=1:K %цикл по числу испытаний
    for i=1:M %цикл по классам
        [x_]=IMS(:, k+K*i-K); %randncor(n,1,C);
        px=3;
        x(1:n,1)=m(i,:)' + x_;%генерация образа i-го класса
        u=G * x+log(pw');%вычисление значения разделяющих функций
        [ui,iai]=max(u);%определение максимума
        Pc_(i,iai)=Pc_(i,iai)+1;%фиксация результата распознавания
        plot3(x_(1), x_(2), x_(3), label{iai});
    end
end
view(3);
Pc_=Pc_/K;
disp('Теоретическая матрица вероятностей ошибок');disp(PIJ); 
disp('Экспериментальная матрица вероятностей ошибок');disp(Pc_);
