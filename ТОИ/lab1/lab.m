a = 100; 
b = 200; 
n = 1000; 

meanDefault = a+b/2;
varDefault = (b^2)/12;

means = 1:n;
vars = 1:n;

% ���������� ���. �������� � ��������� ��� ���������� ����� ���������� �� 
for k = 1 : n 
    distribK = distribution(k, a, b);
	means(k) = mean(distribK);
 	vars(k) = var(distribK);
end

%���������� ������� ����������� ���. �������� �� ����� ���������� ��
plot(1:n, means)
line([0 n],[meanDefault meanDefault], 'Color','red')

%���������� ������� ����������� ��������� �� ����� ���������� ��
plot(1:n, vars)
line([0 n],[varDefault varDefault], 'Color','red')

%���������� ����������� �������� ��
distrib = distribution(n, a, b);
h = histogram(distrib, 'Normalization', 'pdf');
hold on;

%���������� ������� ��������� ������������� ��
syms f(x);
f(x) = piecewise(a<=x<=a+b, 1/b, a+b<x, 0, a>x , 0);
dInterval = (a+b)/10;
fplot(f, [a-dInterval, (a+b) + dInterval])

function r = distribution(numItems, a, b)
	r = arrayfun(@(i) a+b*i, rand(numItems, 1)); 
end