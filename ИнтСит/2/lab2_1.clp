(deftemplate state
    (slot Ivan (type NUMBER))
    (slot Petr (type NUMBER))
    (slot Sidr (type NUMBER))
    (slot success (type SYMBOL) (default No))
)

(defrule IVIPAS
?s <- (state (Ivan ?I) (Petr ?P) (Sidr ?S))
(test
    (and 
        (or (= ?I 1) (= ?S 0))
        (or 
            (and (= ?I 1) (= ?P 0))
            (= ?S 1)
        )
    )
)
=>
(modify ?s (success Yes))
)

(defrule remove
?s <- (state (success No))
=>
(retract ?s)
)

(deffacts world
    (state (Ivan 0) (Petr 0) (Sidr 0))
    (state (Ivan 0) (Petr 0) (Sidr 1))
    (state (Ivan 0) (Petr 1) (Sidr 0))
    (state (Ivan 0) (Petr 1) (Sidr 1))
    (state (Ivan 1) (Petr 0) (Sidr 0))
    (state (Ivan 1) (Petr 0) (Sidr 1))
    (state (Ivan 1) (Petr 1) (Sidr 0))
    (state (Ivan 1) (Petr 1) (Sidr 1))
)