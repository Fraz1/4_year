right_of(X, Y, Max) :- (X is Y+1; X is Y-Max+1).
left_of(X, Y, Max) :- right_of(Y, X, Max).
is_between(X, A, B, Max):- right_of(X, A, Max), left_of(X, B, Max).
is_between(X, A, B, Max):- right_of(X, B, Max), left_of(X, A, Max).


start:- sol(R).
sol(Circle) :- 
    Circle = [
        girl(1, Name1, DressColor1),
        girl(2, Name2, DressColor2),
        girl(3, Name3, DressColor3),
        girl(4, Name4, DressColor4)],
    member(girl(_, nadya, _), Circle),
    member(girl(_, anya, _), Circle),
    
    member(girl(I1, I1Name, green), Circle),
    I1Name \= anya,
    I1Name \= valya,
    
    member(girl(I2, galya, _), Circle),
    member(girl(I3, _, blue), Circle),
    is_between(I1, I2, I3, 4),

    member(girl(I4, _, white), Circle),
    member(girl(I5, valya, _), Circle),
    member(girl(I6, _, pink), Circle),
    is_between(I4, I5, I6, 4)
    . 