shift(maria, 'day').
shift(boris, 'evening').
shift(valentina, 'evening').
shift(sergey, 'evening').

knows(_p1, _p2):- 
    shift(_p1, _time)
    ,shift(_p2, _time)
    ,_p1 \= _p2.


does_sergey_know_boris:- knows(sergey, boris).
who_does_valentina_know(X):- knows(valentina, X).
who_does_maria_know(X):- knows(maria, X).