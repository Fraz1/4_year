(deftemplate transfer
    (slot object (type SYMBOL))
    (slot from (type SYMBOL))
    (slot to (type SYMBOL))
)

(deftemplate in
    (slot object (type SYMBOL))
    (slot location (type SYMBOL))
)

(deftemplate airplane-status
    (slot airplane (type SYMBOL))
    (slot status (type SYMBOL) (default ground))
    (slot max-load (type NUMBER) (default 10))
    (slot current-load (type NUMBER) (default 0))
)

(deftemplate goods-status
    (slot from (type SYMBOL))
    (slot to (type SYMBOL))
    (slot name (type SYMBOL))
    (slot mass (type NUMBER) (default 1))
    (slot status (type SYMBOL) (default waiting))
)

(defrule move
    (transfer (object ?A) (from ?F) (to ?T))
    (in (object ?A) (location ?F))
    (airplane-status (airplane ?A) (status air))
    ?airplane-position <- (in (object ?A) (location ~?T))
    =>
    (modify ?airplane-position (location ?T))
)

(defrule fligh-up
    (transfer (object ?A) (to ?T))
    (in (object ?A) (location ~?T))
    (airplane-status (airplane ?A) (status ground))
    ?st <- (airplane-status (airplane ?A) (status ground))
    =>
    (modify ?st (status air))
)

(defrule flight-down
    (transfer (object ?A) (to ?T))
    (in (object ?A) (location ?T))
    (airplane-status (airplane ?A) (status air))
    ?st <- (airplane-status (airplane ?A) (status air))
    =>
    (modify ?st (status ground))
)

(defrule load
    (declare (salience 100))
    (transfer (object ?A) (from ?F) (to ?T))
    (in (object ?A) (location ?F))

    (airplane-status (airplane ?A) (current-load ?CL) (max-load ?ML) (status ground))
    (goods-status (name ?N) (from ?F) (to ?T) (mass ?M) (status waiting))
    (test (> ?ML (+ ?M ?CL)))
    ?airplane-status <- (airplane-status (airplane ?A) (current-load ?CL) (max-load ?ML) (status ground))
    ?goods-status <- (goods-status (name ?N) (from ?F) (to ?T) (mass ?M) (status waiting))
    ?goods-in <- (in (object ?N) (location ?F))
    
    =>
    (modify ?goods-status (status shipping))
    (modify ?goods-in (location ?A))
    (modify ?airplane-status (current-load (+ ?CL ?M)))
)

(defrule unload
    (declare (salience 100))
    (transfer (object ?A) (from ?F) (to ?T))
    (in (object ?A) (location ?T))

    ?airplane-status <- (airplane-status (airplane ?A) (current-load ?CL) (status ground))
    ?goods-status <- (goods-status (name ?N) (from ?F) (to ?T) (mass ?M) (status shipping))
    ?goods-in <- (in (object ?N) (location ?A))
    =>
    (modify ?goods-status (status shipped))
    (modify ?goods-in (location ?T))
    (modify ?airplane-status (current-load (- ?CL ?M)))
)

(deffacts world
    (in (object airplane1) (location AirPortA))
    (transfer (object airplane1) (from AirPortA) (to AirPortB))
    (airplane-status (airplane airplane1) (status ground))
    
    (in (object airplane2) (location AirPortA))
    (transfer (object airplane2) (from AirPortA) (to AirPortB))
    (airplane-status (airplane airplane2) (status ground) (max-load 20))

    (goods-status (name item1) (from AirPortA) (to AirPortB))
    (in (object item1) (location AirPortA))

    (goods-status (name item2) (from AirPortA) (to AirPortB) (mass 12))
    (in (object item2) (location AirPortA))
)